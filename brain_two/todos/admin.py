from django.contrib import admin
from .models import TodoList, models

# Register your models here.
admin.site.register(models)
admin.site.register(TodoList)