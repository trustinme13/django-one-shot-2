from xml.etree.ElementInclude import include
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path("", todos/, name="todo_list_list"),
path('todos/', <int:pk>/, name="todo_list_detail"),
path("todos/", include("todos.urls")),
path('todos/', <int:pk>/edit/, name="todo_list_update"),
path('todos/', <int:pk>/delete/, name="todo_list_delete"),
path('todos/', "items/create/", name="todo_item_create"),
path('todos/', items/<int:pk>/edit/, name="todo_item_update")
]



